package com.group10.uno;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class astro {
    public static void main(String[] args) throws IOException{

        boolean bot=false;
        // Initialize the UNO game based on number of players
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("How many players? ");
        int numPlayers = Integer.parseInt(br.readLine());

        if(numPlayers==1){
            bot=true;
        }

        System.out.println("What mode of game would you like to play?  2 for progressive, 3 for 7-0 Style, and anything else for a regular game.");
        int gameMode = Integer.parseInt(br.readLine());


        //Launches an alternate game mode.
        if(gameMode==2){
            megami.progressiveGame(numPlayers, br);
            return;
        }
        //Launches an alternate game mode.
        if(gameMode==3){
            megami.sevenOhGame(numPlayers, br);
            return;
        }



        game testGame = new game(numPlayers);
        boolean gameIsRunning = true;
        boolean turnIsRunning = true;
        player currentPlayer = null;
        card selectedCard = null;
        testGame.turn = 1001;

        while (gameIsRunning) {


            if(testGame.loneRanger()){
                gameIsRunning=false;
                continue;  //or break
            }


            currentPlayer = testGame.turnCount(testGame.turn);
            if(currentPlayer.winner){
                testGame.turn++;
                continue;
            }

            turnIsRunning = true;
            System.out.println("Turn " + Integer.toString(testGame.turn));

            // currentPlayer's turn
            while (turnIsRunning) {
                System.out.println("Player " + currentPlayer.playerNumber + "'s turn.\nTop of Discard Pile: " + Arrays.toString(testGame.getTop().code));
                System.out.println("Your hand: ");
                showHand(currentPlayer);

                while(!testGame.legalCheck(currentPlayer.hand, testGame.getTop())){
                    selectedCard = testGame.draw();
                    currentPlayer.drawCard(selectedCard);
                    System.out.println("Drew card: " + Arrays.toString(selectedCard.code));
                }


                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the card number of the card you want to play:");

                String q=br.readLine();
                while(q==""){
                    System.out.println("Invalid.  Try again.");
                    q=br.readLine();
                }

                int cardIndex = Integer.parseInt(q) - 1;
                selectedCard = currentPlayer.getCardAtIndex(cardIndex);

                if (testGame.legalCard(currentPlayer, selectedCard)){

                    System.out.println("Card is legal.");
                    currentPlayer.playCard(selectedCard);
                    turnIsRunning = false;
                }

                else {
                    System.out.println("Illegal card. Choose again.");
                    selectedCard = null;
                    continue;
                }


                System.out.println("Player " + currentPlayer.playerNumber + "'s turn is over.");

                if(currentPlayer.hand.size()==1 && currentPlayer.uno==false){
                    testGame.applyDraw(2, currentPlayer);
                }

                currentPlayer.uno=false;

                if (selectedCard != null) {


                    switch((selectedCard.rank)) {
                        case 'r':
                            testGame.applyReverse();
                            selectedCard = null;
                            break;
                        case 'd':
                            testGame.applyDraw(2, testGame.nextViable());
                            break;
                        case 's':
                            testGame.applySkip();
                            selectedCard = null;
                            break;
                        case '4':
                            if(selectedCard.color=='w'){
                                testGame.applyDraw(4, testGame.nextViable());
                            }
                            break;

                        default:
                            selectedCard = null;
                            break;
                    }
                }
                testGame.applySkip();

                if (testGame.deck.size() <= 15) {
                    testGame.stackCheck(testGame.deck, testGame.discardPile);
                }

                // Dummy game ender debugger for now:
                br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("\nTo end game, type END. Otherwise, type literally anything else.");
                String s = br.readLine();
                if (s.equals("END"))
                    gameIsRunning = false;
            } // END while game is running
        }

    }

    public static void showHand(player player) {
        int i = 1;
        ArrayList<card> hand = player.getHand();
        for (card c: hand) {
            System.out.print(Integer.toString(i) + ": " + Arrays.toString(c.code) + ". ");
            i++;

        }
    }
}
