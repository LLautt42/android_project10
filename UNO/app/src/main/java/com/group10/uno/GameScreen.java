package com.group10.uno;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GameScreen extends AppCompatActivity {

    ListView list;
    ImageView imageView;
    Button unoButton;
    AlertDialog alertDialog;
    AlertDialog wildAlertDialog;
    AlertDialog gameOverDialog;
    boolean unoFlag = false;

    ArrayList<String> itemname = new ArrayList<String>();
    ArrayList<Integer> imgid = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);

        Intent intent = getIntent();
        int numPlayers = intent.getIntExtra("numPlayers", 1);
        int gameMode = intent.getIntExtra("gameMode", 1);
        //game test = new game(numPlayers);

        // Setting UNO card image resource from drawable
        imageView = (ImageView) findViewById(R.id.discard_imageview);
        imageView.setImageResource(R.drawable.start);

        /*
        // ImageView listener
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Clicking", Toast.LENGTH_SHORT).show();
            }
        });
        */

        CustomListAdapter adapter=new CustomListAdapter(this, itemname, imgid);
        list=(ListView)findViewById(R.id.hand_listview);
        list.setAdapter(adapter);



        //passButton = (Button) findViewById(R.id.pass_button);
        unoButton = (Button) findViewById(R.id.uno_button);
        //playButton = (Button) findViewById(R.id.play_button);

        // Alert dialog for turn transitions
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Next Player's Turn");
        alertDialog.setMessage("If you are the player that just played a card, please pass the phone to the next player!\n\n\nIf you are the next player, please press the button whenever you're ready to go!\n\n\nNo cheating, now.\n\n\n\nWe'll know.\n( ͡° ͜ʖ ͡°)");


        // Alert dialog for game over
        gameOverDialog = new AlertDialog.Builder(this).create();
        gameOverDialog.setTitle("Game over!");
        gameOverDialog.setMessage("All but one player has won! Yay!\n\nIf you are that one player, re-evaluate your life choices.");


        switch (gameMode) {
            case 1:
                game test = new game(numPlayers);
                try {
                    standardGame(test);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                progGame test2 = new progGame(numPlayers);
                try {
                    progressiveGame(test2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                sevenGame test5 = new sevenGame(numPlayers);
                try {
                    sevenOhGame(test5);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                game test4 = new game(numPlayers);
                try {
                    standardGame(test4);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    public void standardGame(final game g) throws IOException {
        final game testGame = g;
        testGame.turn = 1001;
        final player currentPlayer = testGame.turnCount(testGame.turn);
        card selectedCard = null;

        refreshView(testGame, currentPlayer);

        // Custom ListView listener
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            if(vix(testGame.playerCycle)==2 && currentPlayer.hand.size()==1){
                AlertDialog.Builder builder2 = new AlertDialog.Builder(GameScreen.this);
                builder2.setMessage("All but one player has won! Yay!\n" +
                        "\n" +
                        "If you are that one player, re-evaluate your life choices.  Close the game and play again!");
                builder2.setCancelable(true);



                AlertDialog alert22 = builder2.create();
                alert22.show();

            }
                String selectedItem= itemname.get(position);
                //Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();

                try {
                    if(legalCheck(testGame,testGame.turnCount(testGame.turn),position)) {
                        play(testGame, testGame.turnCount(testGame.turn), position);
                        itemname.clear();
                        imgid.clear();
                        refreshView(testGame, nextTurn(testGame));
                        alertDialog.show();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        if(currentPlayer.hand.size()==1 && unoFlag==false){
            g.applyDraw(2, currentPlayer);
        }
        unoFlag=false;

        // Button listeners
        /*
        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //alertDialog.show();

            }
        });*/

        unoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unoFlag = true;

                AlertDialog.Builder builder1 = new AlertDialog.Builder(GameScreen.this);
                builder1.setMessage("YOU HAVE DECLARED UNO.\nBe sure to announce this to your friends.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
/*
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //refreshView(testGame, nextTurn(testGame));
                //alertDialog.show();

            }
        });
*/
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Start Turn",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//Toast.makeText(getApplicationContext(), "booop", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });


        gameOverDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Back to Home",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(getApplicationContext(), "booop", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();		                        dialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                    }
                });

    }

    public void progressiveGame(final progGame g) throws IOException {

        final progGame testGame = g;
        testGame.turn = 1001;
        final player currentPlayer = testGame.turnCount(testGame.turn);
        card selectedCard = null;

        refreshProgView(testGame, currentPlayer);

        // Custom ListView listener
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(vix(testGame.playerCycle)==2 && currentPlayer.hand.size()==1){
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(GameScreen.this);
                    builder2.setMessage("All but one player has won! Yay!\n" +
                            "\n" +
                            "If you are that one player, re-evaluate your life choices.  Close the game and play again!");
                    builder2.setCancelable(true);



                    AlertDialog alert22 = builder2.create();
                    alert22.show();

                }
                String selectedItem= itemname.get(position);
                //Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();

                try {
                    if(legalProgCheck(testGame,testGame.turnCount(testGame.turn),position)) {
                        playProg(testGame, testGame.turnCount(testGame.turn), position);
                        itemname.clear();
                        imgid.clear();
                        refreshProgView(testGame, nextProgTurn(testGame));
                        alertDialog.show();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        if(currentPlayer.hand.size()==1 && unoFlag==false){
            g.applyDraw(2, currentPlayer);
        }
        unoFlag=false;

        // Button listeners
        /*
        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.show();

            }
        });*/

        unoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unoFlag = true;

                AlertDialog.Builder builder1 = new AlertDialog.Builder(GameScreen.this);
                builder1.setMessage("YOU HAVE DECLARED UNO.\nBe sure to announce this to your friends.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                //alertDialog.show();

            }
        });
/*
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //refreshProgView(testGame, nextProgTurn(testGame));
                //alertDialog.show();

            }
        });
*/
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Start Turn",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

    }

    public void sevenOhGame(final sevenGame g) throws IOException {

        final sevenGame testGame = g;
        testGame.turn = 1001;
        final player currentPlayer = testGame.turnCount(testGame.turn);
        card selectedCard = null;

        refreshSevenView(testGame, currentPlayer);

        // Custom ListView listener
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(vix(testGame.playerCycle)==2 && currentPlayer.hand.size()==1){
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(GameScreen.this);
                    builder2.setMessage("All but one player has won! Yay!\n" +
                            "\n" +
                            "If you are that one player, re-evaluate your life choices.  Close the game and play again!");
                    builder2.setCancelable(true);



                    AlertDialog alert22 = builder2.create();
                    alert22.show();

                }
                String selectedItem= itemname.get(position);
                //Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();

                try {
                    if(legalSevenCheck(testGame,testGame.turnCount(testGame.turn),position)) {
                        playSeven(testGame, testGame.turnCount(testGame.turn), position);
                        itemname.clear();
                        imgid.clear();
                        refreshSevenView(testGame, nextSevenTurn(testGame));
                        alertDialog.show();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        if(currentPlayer.hand.size()==1 && unoFlag==false){
            g.applyDraw(2, currentPlayer);

            AlertDialog.Builder builder3 = new AlertDialog.Builder(GameScreen.this);
            builder3.setMessage("YOU HAVE NOT DECLARED UNO EVEN THOUGH YOU ONLY HAVE ONE CARD IN THE HAND.  YOUR PENALTY IS DRAWING 2 CARDS.");
            builder3.setCancelable(true);

            builder3.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert33 = builder3.create();
            alert33.show();

        }
        unoFlag=false;

        // Button listeners
        /*
        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //alertDialog.show();

            }
        });
*/
        unoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                unoFlag = true;

                AlertDialog.Builder builder1 = new AlertDialog.Builder(GameScreen.this);
                builder1.setMessage("YOU HAVE DECLARED UNO.\nBe sure to announce this to your friends.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });
/*
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //refreshSevenView(testGame, nextSevenTurn(testGame));
                //alertDialog.show();

            }
        });
*/
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Start Turn",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

    }

    public void refreshView(game g, player p) throws IOException {

        if (g.loneRanger()) {
            gameOverDialog.show();
        }

        if (g.deck.size() <= 15) {
            g.stackCheck(g.deck, g.discardPile);
        }
        g.desperation(p);

        String Player = Integer.toString(p.playerNumber);
        Toast.makeText(getApplicationContext(), "Player " + Player + "\'s turn", Toast.LENGTH_SHORT).show();

        list.invalidateViews();
        // Populate ImageView with the current top of the discard pile
        String dDrawableName = governor.retPic(g.getTop().rank, g.getTop().color);
        int dResID = getResources().getIdentifier(dDrawableName , "drawable", getPackageName());
        imageView.setImageResource(dResID);

        // Populate ListView with the current player's hand
        for (card c : p.hand) {
            itemname.add(c.namer(c.index));

            String mDrawableName = governor.retPic(c.rank, c.color);
            int resID = getResources().getIdentifier(mDrawableName , "drawable", getPackageName());
            imgid.add(resID);
        }
    }

    public void refreshProgView(progGame g, player p) throws IOException {

        if (g.loneRanger()) {
            gameOverDialog.show();
        }

        if (g.deck.size() <= 15) {
            g.stackCheck(g.deck, g.discardPile);
        }
        g.desperation(p);

        String Player = Integer.toString(p.playerNumber);
        Toast.makeText(getApplicationContext(), "Player " + Player + "\'s turn", Toast.LENGTH_SHORT).show();

        list.invalidateViews();
        // Populate ImageView with the current top of the discard pile
        String dDrawableName = governor.retPic(g.getTop().rank, g.getTop().color);
        int dResID = getResources().getIdentifier(dDrawableName , "drawable", getPackageName());
        imageView.setImageResource(dResID);

        // Populate ListView with the current player's hand
        for (card c : p.hand) {
            itemname.add(c.namer(c.index));

            String mDrawableName = governor.retPic(c.rank, c.color);
            int resID = getResources().getIdentifier(mDrawableName , "drawable", getPackageName());
            imgid.add(resID);
        }
    }

    public void refreshSevenView(sevenGame g, player p) throws IOException {

        if (g.loneRanger()) {
            gameOverDialog.show();
        }

        if (g.deck.size() <= 15) {
            g.stackCheck(g.deck, g.discardPile);
        }
        g.desperation(p);

        String Player = Integer.toString(p.playerNumber);
        Toast.makeText(getApplicationContext(), "Player " + Player + "\'s turn", Toast.LENGTH_SHORT).show();

        list.invalidateViews();
        // Populate ImageView with the current top of the discard pile
        String dDrawableName = governor.retPic(g.getTop().rank, g.getTop().color);
        int dResID = getResources().getIdentifier(dDrawableName , "drawable", getPackageName());
        imageView.setImageResource(dResID);

        // Populate ListView with the current player's hand
        for (card c : p.hand) {
            itemname.add(c.namer(c.index));

            String mDrawableName = governor.retPic(c.rank, c.color);
            int resID = getResources().getIdentifier(mDrawableName , "drawable", getPackageName());
            imgid.add(resID);
        }
    }

    public player nextTurn(game g) {
        g.applySkip();
        return g.turnCount(g.turn);
    }

    public player nextProgTurn(progGame g) {
        g.applySkip();
        return g.turnCount(g.turn);
    }

    public player nextSevenTurn(sevenGame g) {
        g.applySkip();
        return g.turnCount(g.turn);
    }

    public boolean legalCheck(game g, player p, int index) throws IOException{
        card c = p.getCardAtIndex(index);
        if (g.legalCard(p, c))
            return true;
        else {
            Toast.makeText(getApplicationContext(), "Illegal move", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean legalProgCheck(progGame g, player p, int index) throws IOException{
        card c = p.getCardAtIndex(index);
        if (g.legalCard(p, c))
            return true;
        else {
            Toast.makeText(getApplicationContext(), "Illegal move", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean legalSevenCheck(sevenGame g, player p, int index) throws IOException{
        card c = p.getCardAtIndex(index);
        if (g.legalCard(p, c))
            return true;
        else {
            Toast.makeText(getApplicationContext(), "Illegal move", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void play(game g, player p, int index) {
        card c = p.getCardAtIndex(index);
        p.playCard(c);

        switch(c.rank) {
            case 'r':
                g.applyReverse();
                break;
            case 'd':
                int t=g.turn;
                g.applyDraw(2, nextTurn(g));
                g.turn=t;
                break;
            case 's':
                g.applySkip();
                break;
            case '4':
                if (c.color == 'w') {
                    int u=g.turn;
                    g.applyDraw(2, nextTurn(g));
                    g.turn=u;
                }
                break;
        }

        if (p.hand.size() == 0) {
            if (vix(g.playerCycle) == 2) {
                gameOverDialog.show();
            }
            p.winner = true;
        }
    }

    public void playProg(progGame g, player p, int index) {
        card c = p.getCardAtIndex(index);
        p.playCard(c);

        int q=g.comboCount * 2;

        switch(c.rank) {
            case 'r':
                g.applyReverse();
                g.comboCount=0;
                break;
            case 'd':
                int t=g.turn;
                g.applyDraw(2+q, nextProgTurn(g));
                g.turn=t;
                g.comboCount++;
                break;
            case 's':
                g.applySkip();
                g.comboCount=0;
                break;
            case '4':
                if (c.color == 'w') {
                    int u=g.turn;
                    g.applyDraw(2, nextProgTurn(g));
                    g.turn=u;
                }
                g.comboCount=0;
                break;
            default:
                break;
        }
        if (p.hand.size() == 0) {
            if (vix(g.playerCycle) == 2) {
                gameOverDialog.show();
            }
            p.winner = true;
        }
    }

    public void playSeven(sevenGame g, player p, int index) {
        card c = p.getCardAtIndex(index);
        p.playCard(c);

        switch(c.rank) {
            case 'r':
                g.applyReverse();
                break;
            case 'd':
                int t=g.turn;
                g.applyDraw(2, nextSevenTurn(g));
                g.turn=t;
                break;
            case 's':
                g.applySkip();
                break;
            case '4':
                if (c.color == 'w') {
                    int u=g.turn;
                    g.applyDraw(4, nextSevenTurn(g));
                    g.turn=u;
                }
                break;
            case '0':
            case '7':
                governor.roundRobin(g.playerCycle);
                break;




        }

        if (p.hand.size() == 0) {
            if (vix(g.playerCycle) == 2) {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(GameScreen.this);
                builder2.setMessage("All but one player has won! Yay!\n" +
                        "\n" +
                        "If you are that one player, re-evaluate your life choices.  Close the game and play again!");
                builder2.setCancelable(true);



                AlertDialog alert22 = builder2.create();
                alert22.show();
            }
            p.winner = true;
        }

    }
    public int vix(ArrayList<player> ps){
        int q=0;
        for(player p: ps){
            if(p.winner==false){
                q++;
            }
        }
        return q;

    }
}
