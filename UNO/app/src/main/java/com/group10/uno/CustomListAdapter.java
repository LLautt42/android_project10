package com.group10.uno;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> itemName;
    private final ArrayList<Integer> imageName;

    public CustomListAdapter(Activity context, ArrayList<String> itemName, ArrayList<Integer> imageName) {
        super(context, R.layout.hand_list, itemName);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemName=itemName;
        this.imageName=imageName;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.hand_list, null,true);

        TextView cardTextView = (TextView) rowView.findViewById(R.id.cardName);
        ImageView cardImageView = (ImageView) rowView.findViewById(R.id.cardIcon);

        cardTextView.setText(itemName.get(position));
        cardImageView.setImageResource(imageName.get(position));
        return rowView;

    };

}
