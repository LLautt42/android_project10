package com.group10.uno;

public class card {
	char rank;
	char color;
	int index;
	char[] code;
	String desc;
	public card(char rank, char color, int index){
		this.rank=rank;
		this.color=color;
		this.index=index;
		this.code=transformer(index);
		this.desc=namer(index);
	}

	boolean isLegal(card c){
		if(c.rank==this.rank || c.color==this.color){
			return true;
		}
		return false;
	}

	public static String namer(int index){
		switch(index){
			case 1:
				return "Red 0";

			case 2:
				return "Yellow 0";

			case 3:
				return "Green 0";

			case 4:
				return "Blue 0";

			case 5:
			case 6:
				return "Red 1";

			case 7:
			case 8:
				return "Yellow 1";

			case 9:
			case 10:
				return "Green 1";

			case 11:
			case 12:
				return "Blue 1";

			case 13:
			case 14:
				return "Red 2";

			case 15:
			case 16:
				return "Yellow 2";

			case 17:
			case 18:
				return "Green 2";

			case 19:
			case 20:
				return "Blue 2";

			case 21:
			case 22:
				return "Red 3";

			case 23:
			case 24:
				return "Yellow 3";

			case 25:
			case 26:
				return "Green 3";

			case 27:
			case 28:
				return "Blue 3";

			case 29:
			case 30:
				return "Red 4";

			case 31:
			case 32:
				return "Yellow 4";

			case 33:
			case 34:
				return "Green 4";

			case 35:
			case 36:
				return "Blue 4";

			case 37:
			case 38:
				return "Red 5";

			case 39:
			case 40:
				return "Yellow 5";

			case 41:
			case 42:
				return "Green 5";

			case 43:
			case 44:
				return "Blue 5";

			case 45:
			case 46:
				return "Red 6";

			case 47:
			case 48:
				return "Yellow 6";

			case 49:
			case 50:
				return "Green 6";

			case 51:
			case 52:
				return "Blue 6";

			case 53:
			case 54:
				return "Red 7";

			case 55:
			case 56:
				return "Yellow 7";

			case 57:
			case 58:
				return "Green 7";

			case 59:
			case 60:
				return "Blue 7";

			case 61:
			case 62:
				return "Red 8";

			case 63:
			case 64:
				return "Yellow 8";

			case 65:
			case 66:
				return "Green 8";

			case 67:
			case 68:
				return "Blue 8";

			case 69:
			case 70:
				return "Red 9";

			case 71:
			case 72:
				return "Yellow 9";

			case 73:
			case 74:
				return "Green 9";

			case 75:
			case 76:
				return "Blue 9";

			case 77:
			case 78:
				return "Red Skip";

			case 79:
			case 80:
				return "Yellow Skip";

			case 81:
			case 82:
				return "Green Skip";

			case 83:
			case 84:
				return "Blue Skip";

			case 85:
			case 86:
				return "Red Draw2";

			case 87:
			case 88:
				return "Yellow Draw2";

			case 89:
			case 90:
				return "Green Draw2";

			case 91:
			case 92:
				return "Blue Draw2";

			case 93:
			case 94:
				return "Red Reverse";

			case 95:
			case 96:
				return "Yellow Reverse";

			case 97:
			case 98:
				return "Green Reverse";

			case 99:
			case 100:
				return "Blue Reverse";

			case 101:
			case 102:
			case 103:
			case 104:
				return "Red 0";

			case 105:
			case 106:
				return "Red 4";

			default:
				return "NULL.";
		}
	}


	public static char[] transformer(int index){
		char[] dux=new char[2];
		switch(index){
			case 1:
				dux[0]='r';
				dux[1]='0';
				break;
			case 2:
				dux[0]='y';
				dux[1]='0';
				break;
			case 3:
				dux[0]='g';
				dux[1]='0';
				break;
			case 4:
				dux[0]='b';
				dux[1]='0';
				break;
			case 5:
			case 6:
				dux[0]='r';
				dux[1]='1';
				break;
			case 7:
			case 8:
				dux[0]='y';
				dux[1]='1';
				break;
			case 9:
			case 10:
				dux[0]='g';
				dux[1]='1';
				break;
			case 11:
			case 12:
				dux[0]='b';
				dux[1]='1';
				break;
			case 13:
			case 14:
				dux[0]='r';
				dux[1]='2';
				break;
			case 15:
			case 16:
				dux[0]='y';
				dux[1]='2';
				break;
			case 17:
			case 18:
				dux[0]='g';
				dux[1]='2';
				break;
			case 19:
			case 20:
				dux[0]='b';
				dux[1]='2';
				break;
			case 21:
			case 22:
				dux[0]='r';
				dux[1]='3';
				break;
			case 23:
			case 24:
				dux[0]='y';
				dux[1]='3';
				break;
			case 25:
			case 26:
				dux[0]='g';
				dux[1]='3';
				break;
			case 27:
			case 28:
				dux[0]='b';
				dux[1]='3';
				break;
			case 29:
			case 30:
				dux[0]='r';
				dux[1]='4';
				break;
			case 31:
			case 32:
				dux[0]='y';
				dux[1]='4';
				break;
			case 33:
			case 34:
				dux[0]='g';
				dux[1]='4';
				break;
			case 35:
			case 36:
				dux[0]='b';
				dux[1]='4';
				break;
			case 37:
			case 38:
				dux[0]='r';
				dux[1]='5';
				break;
			case 39:
			case 40:
				dux[0]='y';
				dux[1]='5';
				break;
			case 41:
			case 42:
				dux[0]='g';
				dux[1]='5';
				break;
			case 43:
			case 44:
				dux[0]='b';
				dux[1]='5';
				break;
			case 45:
			case 46:
				dux[0]='r';
				dux[1]='6';
				break;
			case 47:
			case 48:
				dux[0]='y';
				dux[1]='6';
				break;
			case 49:
			case 50:
				dux[0]='g';
				dux[1]='6';
				break;
			case 51:
			case 52:
				dux[0]='b';
				dux[1]='6';
				break;
			case 53:
			case 54:
				dux[0]='r';
				dux[1]='7';
				break;
			case 55:
			case 56:
				dux[0]='y';
				dux[1]='7';
				break;
			case 57:
			case 58:
				dux[0]='g';
				dux[1]='7';
				break;
			case 59:
			case 60:
				dux[0]='b';
				dux[1]='7';
				break;
			case 61:
			case 62:
				dux[0]='r';
				dux[1]='8';
				break;
			case 63:
			case 64:
				dux[0]='y';
				dux[1]='8';
				break;
			case 65:
			case 66:
				dux[0]='g';
				dux[1]='8';
				break;
			case 67:
			case 68:
				dux[0]='b';
				dux[1]='8';
				break;
			case 69:
			case 70:
				dux[0]='r';
				dux[1]='9';
				break;
			case 71:
			case 72:
				dux[0]='y';
				dux[1]='9';
				break;
			case 73:
			case 74:
				dux[0]='g';
				dux[1]='9';
				break;
			case 75:
			case 76:
				dux[0]='b';
				dux[1]='9';
				break;
			case 77:
			case 78:
				dux[0]='r';
				dux[1]='s';
				break;
			case 79:
			case 80:
				dux[0]='y';
				dux[1]='s';
				break;
			case 81:
			case 82:
				dux[0]='g';
				dux[1]='s';
				break;
			case 83:
			case 84:
				dux[0]='b';
				dux[1]='s';
				break;
			case 85:
			case 86:
				dux[0]='r';
				dux[1]='d';
				break;
			case 87:
			case 88:
				dux[0]='y';
				dux[1]='d';
				break;
			case 89:
			case 90:
				dux[0]='g';
				dux[1]='d';
				break;
			case 91:
			case 92:
				dux[0]='b';
				dux[1]='d';
				break;
			case 93:
			case 94:
				dux[0]='r';
				dux[1]='r';
				break;
			case 95:
			case 96:
				dux[0]='y';
				dux[1]='r';
				break;
			case 97:
			case 98:
				dux[0]='g';
				dux[1]='r';
				break;
			case 99:
			case 100:
				dux[0]='b';
				dux[1]='r';
				break;
			case 101:
			case 102:
			case 103:
			case 104:
				dux[0]='r';
				dux[1]='0';
				break;
			case 105:
			case 106:
				dux[0]='r';
				dux[1]='4';
				break;

		}

		return dux;
	}
}
