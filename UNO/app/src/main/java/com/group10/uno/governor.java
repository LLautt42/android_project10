package com.group10.uno;

import java.util.ArrayList;

public class governor {


    public static int returnPlayer(ArrayList<player> people, int num){
        for (player temp: people){
            if(temp.playerNumber==num){
                return people.indexOf(temp);
            }
        }
        return -1;

    }



    public void switcheroo(player player1, player player2){
        ArrayList<card> temp1=player1.hand;
        ArrayList<card> temp2=player2.hand;

        player1.hand=temp2;
        player2.hand=temp1;
        return;
    }

    //Returns an array of the order of players.
    public static int[] playerCheck(ArrayList<player> players){

        int count=0;
        for(player p: players){
            if(p.winner==false){
                count++;
            }
        }
        int temp[]=new int[count];
        int subcount=0;
        for(player p: players){
            if(p.winner==false){
                temp[subcount]=p.playerNumber;
                subcount++;
            }
        }


        return temp;
    }

    public static void roundRobin(ArrayList<player> players){
        int playFlow[]=playerCheck(players);

        //ArrayList<ArrayList<card>> modPlayersHands=new ArrayList<ArrayList<card>>();
        ArrayList<player> modPlayers=new ArrayList<player>();
        for(int q=0; q<playFlow.length; q++){
            modPlayers.add(players.get(playFlow[q]-1));
        }
        if(modPlayers.size()==2){
            ArrayList<card> temp1=modPlayers.get(0).hand;
            ArrayList<card> temp2=modPlayers.get(1).hand;

            modPlayers.get(0).hand=temp2;
            modPlayers.get(1).hand=temp1;
            return;
        }
        else if(modPlayers.size()==3){
            ArrayList<card> temp1=modPlayers.get(0).hand;
            ArrayList<card> temp2=modPlayers.get(1).hand;
            ArrayList<card> temp3=modPlayers.get(2).hand;


            modPlayers.get(0).hand=temp3;
            modPlayers.get(1).hand=temp2;
            modPlayers.get(2).hand=temp1;
            return;
        }
        else if(modPlayers.size()==4){
            ArrayList<card> temp1=modPlayers.get(0).hand;
            ArrayList<card> temp2=modPlayers.get(1).hand;
            ArrayList<card> temp3=modPlayers.get(2).hand;
            ArrayList<card> temp4=modPlayers.get(3).hand;

            modPlayers.get(0).hand=temp4;
            modPlayers.get(1).hand=temp3;
            modPlayers.get(2).hand=temp2;
            modPlayers.get(3).hand=temp1;
            return;
        }
        else{
            return;
        }
    }

    public static String retPic(char rank, char color){
        String col;
        String path;

        switch(color){
            case 'r':
                col="red";
                break;
            case 'y':
                col="yellow";
                break;
            case 'g':
                col="green";
                break;
            case 'b':
                col="blue";
                break;
            case 'w':
                col="wild";
                break;
            default:
                return "start";

        }

        path=col+"_"+rank+"_"+"0";
        return path;

    }

}












