package com.group10.uno;
import java.util.ArrayList;

public class baseGenos {

	public player shell;
	public player opponent;

	public baseGenos(player shell, player opponent){
		this.shell=shell;
		this.opponent=opponent;
	}

	public void makeMove(game g){
		boolean released=false;
		while(released==false){
			if(containsMove(shell.hand, g.discardPile, g)){
				card c=firstLegal(shell.hand, g.acceptableColor, g.discardPile.get(0).rank);
				if(c.color=='w' && c.rank!=4){
					g.acceptableColor='r';
				}
				if(c.color=='w' && c.rank==4){
					g.acceptableColor='r';
					g.applyDraw(4, opponent);
				}
				if(c.rank=='d'){
					g.applyDraw(2, opponent);
				}
				if(c.rank=='s'){
					g.applySkip();
				}
				if(c.rank=='r'){
					g.applyReverse();
				}
				shell.playCard(c);
				released=true;
			}
			else{
				shell.hand.add(0, g.draw());
			}
		}

		//Next turn;;

	}

	public boolean containsMove(ArrayList<card> hand, ArrayList<card> discardPile, game g){
		card c=discardPile.get(0);
		if(containsColor(hand, g.acceptableColor)){
			return true;
		}
		if(containsRank(hand, c.rank) && c.color != 'w'){
			return true;
		}

		return false;
	}
	public boolean containsColor(ArrayList<card> hand, char color){
		for(card c: hand){
			if(c.color==color){
				return true;
			}
		}
		return false;
	}
	public boolean containsRank(ArrayList<card> hand, char rank){
		for(card c: hand){
			if(c.rank==rank){
				return true;
			}
		}
		return false;
	}

	public card firstLegal(ArrayList<card> hand, char col, char rnk){
		for(card c: hand){
			if((c.rank==rnk || c.color==col) || c.color=='w'){
				return c;
			}
		}
		card d=new card('y', '5', 40);
		return d;
	}

}

