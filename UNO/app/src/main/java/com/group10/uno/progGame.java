package com.group10.uno;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class progGame {
	int players;
	int turn;
	static ArrayList<card> deck;
	static ArrayList<card> discardPile;
	static ArrayList<player> playerCycle;
	static char acceptableColor;
	int comboCount;

	public progGame(int players){
		this.players=players;
		deck=new ArrayList<card>();
		discardPile=new ArrayList<card>();
		deck=shuffle(deck, discardPile, 0);
		this.turn=0;
		this.comboCount=0;

		playerCycle=new ArrayList<player>();
		for (int i=0; i<=players; i++) {
			playerCycle.add(new player(i, deck, discardPile));
		}
	}

	public static boolean loneRanger(){
		int c=0;
		for(player p: playerCycle){
			if(!p.winner){
				c++;
			}
		}
		if(c==1)
			return true;
		else
			return false;
	}


	public static ArrayList<card> shuffle(ArrayList<card> deck, ArrayList<card> discardPile, int flag){
		if(flag==0){
			deck.clear();

			for(int q=1; q<107; q++){
				char[] info=card.transformer(q);
				card c= new card(info[1], info[0], q);
				deck.add(c);

			}

		}
		long seed=System.nanoTime();
		Collections.shuffle(deck, new Random(seed));

		discardPile.clear();
		discardPile.add(0, deck.get(0));
		acceptableColor=discardPile.get(0).color;
		deck.remove(0);

		return deck;
	}

	public ArrayList<card> stackCheck(ArrayList<card> deck, ArrayList<card> discard){

		int count=0;
		card temp=new card('0', '0', 0);
		while(count<discard.size() && discard.get(count)!=null){
			temp=discard.get(count);
			deck.add(temp);
			count++;
		}
		shuffle(deck, discard, 1);

		return deck;
	}
	public static card draw(){
		card c=deck.get(0);
		deck.remove(0);
		return c;

	}

	public boolean legalCheck(ArrayList<card> hand, card c){
		for(card q: hand){
			if(q.rank==c.rank || q.color==c.color || q.color=='w'){
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the card at the top of the discard pile
	 *
	 * @return the card at the top of the discard pile
	 */
	public static card getTop() {
		return discardPile.get(0);
	}

	public boolean legalCard(player p, card c) throws IOException{
		card temp=getTop();
		char temp1=temp.color;
		char temp2=temp.rank;
		if(c.color=='w'){
			Scanner numPlayersReader = new Scanner(System.in);
			System.out.println("What color? ");
			acceptableColor = (char) System.in.read();

			return true;
		}
		else if(temp1!='w'){
			if(c.color==temp1 || c.rank==temp2){
				acceptableColor=c.color;
				return true;
			}
			return false;
		}
		else{
			if(c.color==acceptableColor){
				acceptableColor=c.color;
				return true;
			}
			return false;

		}
	}
	public void applyDraw(int num, player p){
		int count=0;
		while(count<=num){
			p.hand.add(0, draw());
			count++;
		}

	}
	public void applySkip(){
		turn++;
		if(turnCount(turn).winner==true){
			while(turnCount(turn).winner==true){
				turn++;
			}
			turn++;
			return;
		}
		return;
	}

	public void applyReverse(){
		if(players==2){
			applySkip();

		}
		turn = turn * -1;
		return;

	}

	public player nextViable(){
		int count=1;
		while(true){

			if(turnCount(turn+count).winner==false){
				return turnCount(turn+count);
			}

			count++;
		}
	}

	public player turnCount(int turn){
		player temp=playerCycle.get(0);
		while(true){
			if(players==2){
				//Player 2
				if(Math.abs(turn)%2==0){
					return playerCycle.get(governor.returnPlayer(playerCycle, 2));
				}
				//Player 1
				else{


					return playerCycle.get(governor.returnPlayer(playerCycle, 1));
				}

			}
			if(players==3){
				//Player 3
				if(Math.abs(turn)%3==0){
					if(playerCycle.get(governor.returnPlayer(playerCycle, 3)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 3));
					}

				}
				//Player2
				else if(Math.abs(turn)%3==2){
					if(playerCycle.get(governor.returnPlayer(playerCycle, 2)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 2));
					}

				}
				//Player1
				else{
					if(playerCycle.get(governor.returnPlayer(playerCycle, 1)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 1));
					}

				}

			}
			else{
				//Player 4
				if(Math.abs(turn)%4==0){
					if(playerCycle.get(governor.returnPlayer(playerCycle, 4)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 4));
					}
				}
				//Player3
				else if(Math.abs(turn)%4==3){
					if(playerCycle.get(governor.returnPlayer(playerCycle, 3)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 3));
					}
				}
				//Player2
				else if(Math.abs(turn)%4==2){
					if(playerCycle.get(governor.returnPlayer(playerCycle, 2)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 2));
					}
				}
				//Player1
				else{
					if(playerCycle.get(governor.returnPlayer(playerCycle, 1)).winner==true){
						turn++;
						continue;
					}
					else{
						return playerCycle.get(governor.returnPlayer(playerCycle, 1));
					}
				}
			}
		}

	}

	public int handCheck(player p){
		if(p.hand.size()==1){
			return 1;
		}
		if(p.hand.size()==0){
			return 0;
		}
		else{
			return -1;
		}
	}


	public int comboCounter(card c){
		if(c.rank=='d'){
			comboCount++;
			return 2*comboCount;
		}
		else{
			comboCount=0;
			return 0;
		}
	}

	public static void desperation(player p){
		card c=getTop();
		while(true){
			for(card q:p.hand){
				if(c.rank==q.rank || c.color==q.color || q.color=='w'){
					return;
				}
			}
			p.hand.add(draw());
		}
	}

	public int vix(ArrayList<player> ps){
		int q=0;
		for(player p: ps){
			if(p.winner==false){
				q++;
			}
		}
		return q;

	}


}
