package com.group10.uno;

import java.util.ArrayList;

public class player {
	int playerNumber;
	ArrayList<card> deck = new ArrayList<card>();
	ArrayList<card> hand = new ArrayList<card>();
	ArrayList<card> discard = new ArrayList<card>();
	static boolean winner;
	static boolean uno;

	public player(int num, ArrayList<card> Deck, ArrayList<card> discard){
		this.playerNumber=num;
		this.deck=Deck;
		this.discard=discard;
		this.winner=false;
		genesis(deck);
		this.uno=false;
	}

	public static boolean victory(ArrayList<card> hand){
		if(hand.size()==0){
			winner=true;
			return true;
		}
		return false;
	}

	public void genesis(ArrayList<card> deck){
		for (int i=0; i<7; i++){
			this.hand.add(deck.get(0));
			deck.remove(0);
		}
		return;
	}

	public void playCard(card c){
		this.discard.add(0, c);
		this.hand.remove(c);
		return;
	}

	public void drawCard(card c) {
		this.hand.add(c);
	}

	public ArrayList<card> getHand() {
		return hand;
	}

	public card getCardAtIndex(int i) {
		if(this.hand.get(i)==null){
			return null;
		}
		return this.hand.get(i);
	}

}
