package com.group10.uno;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * The Home Screen allows users to select options for their desired game of UNO.
 *
 * Options include: - The number of human players that will play
 *                  - The set of house rules
 */
public class HomeScreen extends AppCompatActivity {

    private RadioGroup radioGroup; // Toggles number of players
    private Spinner spinner; // Toggles set of house rules
    private Button button; // Launches game of UNO based on toggled options
    int pos;
    int numPlayers = 1; // Used to pass number of players to Game Screen
    int gameMode = 1; // Used to pass set of house rules to Game Screen
    ArrayAdapter<CharSequence> adapter;
    AlertDialog alertDiag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        radioGroup = (RadioGroup) findViewById(R.id.players_radio);
        spinner = (Spinner) findViewById(R.id.mode_spinner);
        button = (Button) findViewById(R.id.launch_button);

        // Radio button listener
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                pos = radioGroup.indexOfChild(findViewById(checkedId));

                switch(pos) {
                    case 0: // 4-player game with one human player selected
                        adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.mode_onePlayer_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        numPlayers = 1;
                        break;
                    case 1: // 2-player game with two human players selected
                        adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.mode_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        numPlayers = 2;
                        break;
                    case 2: // 3-player game with three human players selected
                        adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.mode_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        numPlayers = 3;
                        break;
                    case 3: // 4-player game with four human players selected
                        adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.mode_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        numPlayers = 4;
                        break;
                    default: // Default game is a 4-player game with one human player
                        adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.mode_array, android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                        numPlayers = 1;
                        break;
                }
            }
        });

        // Spinner listener
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String item = parent.getItemAtPosition(position).toString();
                        switch(item) {
                            case "Standard UNO":
                                gameMode = 1;
                                break;
                            case "Progressive UNO":
                                gameMode = 2;
                                break;
                            case "Seven-0 UNO":
                                gameMode = 3;
                                break;
                            default: // Default house rules are those of Standard UNO
                                gameMode = 1;
                                break;
                        }
                    }
                    public void onNothingSelected(AdapterView<?> arg0) { }
                });

        // Button listener
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GameScreen.class);
                if(numPlayers!=1) {
                    intent.putExtra("numPlayers", numPlayers); // Applies setting for number of players to game launch
                    intent.putExtra("gameMode", gameMode); // Applies setting for house rules to game launch
                    startActivity(intent);
                }

                else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeScreen.this);
                    builder1.setMessage("You cannot play UNO by yourself!  It would take a person years to make a smart AI for this.  We don't want Skynet to take over Mattel, a beloved gaming icon for years on end, do we?  No.  I didn't think so.  Choose another amount of players.");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }
        });


    }
}