Logan Lautt and Angeline Tamayo
Roll Your Own Project- UNO
5/5/2016

To open up the game , simply open up Android Studio, select File->Open, then navigate to our UNO file and open it.
The code should show up and the gradle should be built.  Afterward, simply run the game on an emulator.

The game starts with the choice of either two, three, or four players, since playing UNO solo is imossible!  You are then shown a screen with the top card of the stack and your hand in a neat array list.  Simply tap the card you want to play!  Once you do, just hand it to the next person and repeat the process until a winner is declared.  In each instance of a game, there is also an option to play either Progressive Style or Seven-Zero UNO, a cool way to spice up your game play.

Good luck, and have fun!